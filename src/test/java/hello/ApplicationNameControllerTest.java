package hello;

import org.junit.Assert;
import org.junit.Test;

public class ApplicationNameControllerTest {

    @Test
    public void shouldReturnApplicationName() {
        ApplicationNameController applicationNameController = new ApplicationNameController();
        ApplicationName applicationName = applicationNameController.applicationName();
        Assert.assertEquals("BookApplication", applicationName.getName());
    }

}
