package hello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationNameController {

    @GetMapping("/application")
    public ApplicationName applicationName() {
        return new ApplicationName("BookApplication");
    }
}
